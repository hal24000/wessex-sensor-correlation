from typing import List

import networkx as nx
import pandas as pd

from setup.database import db_con


db = db_con()


def compare_edges(nodes: List[str]) -> str:
    """Compares two nodes and returns the node with more connections. Used to decide which node to remove in a network
    
    Args:
        nodes: List of nodes
        
    Returns:
        Node to remove
    """
    DG = make_nx_graph()
    node_zero = len(DG.in_edges(nodes[0])) + len(DG.out_edges(nodes[0]))
    node_one = len(DG.in_edges(nodes[1])) + len(DG.out_edges(nodes[1]))
    if node_zero > node_one:
        return nodes[0]
    elif node_one > node_zero:
        return nodes[1]
    elif node_one == node_zero:
        return nodes[1]


def make_nx_graph() -> nx.classes.digraph.DiGraph:
    """
    Makes networkx graph from CSV with information on all nodes
    Info: Site ID, type of node, downstream nodes, storm tank, inflow, redacted XY coordinates, lat/lon coordinates, spill levels (mm)

    Returns:
        Directed graph
    """
    df = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0}))
    DG = nx.DiGraph()
    for i, row in df.iterrows():
        site_id = row["Site_ID"]
        DG.add_node(
            site_id,
            node_type=row["type"],
            storm_tank=row["storm_tank"],
            inflow=row["inflow"],
            x=row["X_redacted"],
            y=row["Y_redacted"],
        )
        if row["down_stream"] != "0":
            DG.add_edge(
                site_id,
                row["down_stream"],
            )
    return DG


def check_connection(nodes: List[str]) -> str:
    """Determines whether there is a path between two nodes
    
    Args:
        nodes: List of nodes
        
    Returns:
        Connection length between two nodes"""
    DG = make_nx_graph()
    if nx.has_path(DG, nodes[0], nodes[1]):
        return str(nx.shortest_path_length(DG, nodes[0], nodes[1]))
    elif nx.has_path(DG, nodes[1], nodes[0]):
        return str(nx.shortest_path_length(DG, nodes[1], nodes[0]))
    else:
        return "-"
