from typing import Tuple

import matplotlib
import networkx as nx
import numpy as np
import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from setup.database import db_con
from model.features import get_cso_cluster
from model.graph_features import make_nx_graph

db = db_con()


def draw_correlation_table(
    data_select: str, connected: str, metric: str, cutoff: float
) -> Tuple[pd.DataFrame, np.ndarray]:
    """Creates dataframe of pairwise correlations
    
    Args:
        data_select: Resampling time
        connected: Whether to only consider nodes that have a valid connected path between them
        metric: Metric to sort the data by
        cutoff: Cutoff for the metric
        
    Returns:
        Tuple of output correlation dataframe, and the number of nodes proposed for removal
    """
    if data_select == "60m":
        df = pd.DataFrame(db["kk_sensor_correlation_60m"].find({}, {"_id": 0}))
    elif data_select == "15m":
        df = pd.DataFrame(db["kk_sensor_correlation_15m"].find({}, {"_id": 0}))
    if connected == "Connected":
        df = df[df["Connect"].apply(lambda x: type(x) == int)]

    df = df.sort_values(metric, ascending=False)
    df = df[df[metric] > cutoff]
    return df, df["Min Connect"].unique()


def plot_cso_levels(df: pd.DataFrame, cso_vis_list: str) -> matplotlib.figure.Figure:
    """Plots CSO levels for nodes
    
    Args:
        df: Dataframe of multiple CSOs to plot
        cso_vis_list: List of CSOs to visualize
        
    Returns:
        Subplot figure of CSO levels
    """
    if len(cso_vis_list) == 0:
        return {}
    fig = make_subplots(
        rows=len(cso_vis_list),
        cols=1,
        subplot_titles=cso_vis_list,
        shared_xaxes=True,
        vertical_spacing=0.1,
    )
    for i in range(len(cso_vis_list)):
        fig.add_trace(
            go.Scatter(x=df.index, y=df[cso_vis_list[i]], name=cso_vis_list[i]),
            row=i + 1,
            col=1,
        )
    fig.update_layout(
        margin=dict(l=20, r=20, b=0, t=20),
        height=len(cso_vis_list) * 172,
        showlegend=False,
    )
    return fig


def plot_map_with_proposed(
    df_corr: pd.DataFrame, metric: str, cutoff: float, cso_select: str
) -> plotly.graph_objects.Figure:
    """Plot map with proposed CSO nodes to remove
    
    Args:
        df_corr: Dataframe of correlations
        metric: Chosen metric
        cutoff: Cutoff value
        cso_select: Selected node to be visualized on map
        
    Returns:
        Map with nodes segmented by color
    """
    df = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0}))
    nodes_to_remove = list(set(df_corr[df_corr[metric] > cutoff]["Min Connect"]))
    df["Label"] = np.where(
        df["Site_ID"].isin(nodes_to_remove) == True, "Remove", "Maintain"
    )
    for cso in cso_select:
        df.loc[(df.iloc[:, df.columns.get_loc("Site_ID")] == cso), "Label"] = "Selected"
    fig = px.scatter_mapbox(
        df,
        lat="lat",
        lon="lon",
        hover_name="Site_ID",
        color="Label",
        color_discrete_map={
            "Remove": "teal",
            "Maintain": "lightgrey",
            "Selected": "red",
        },
        zoom=10,
        height=306,
        width=1400,
    )
    fig.update_layout(
        mapbox_style="carto-positron",
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
        showlegend=True,
        legend_title_text="",
        legend=dict(
            x=0,
            y=0.10,
            traceorder="reversed",
            orientation="h",
            bgcolor="rgba(0,0,0,0)",
            font=dict(size=13),
        ),
    )
    return fig


def plot_network(target: str) -> plotly.graph_objects.Figure:
    _, _, cso_cluster = get_cso_cluster(target)
    DG = make_nx_graph()

    node_x = []
    node_y = []
    node_types = []
    for node in DG.nodes():
        x, y = DG.nodes[node]["x"], DG.nodes[node]["y"]
        node_x.append(x)
        node_y.append(y)
        if node in cso_cluster:
            node_type = 1
            node_types.append(node_type)
        else:
            node_type = 0.5
            node_types.append(node_type)

    node_info = []
    for node in DG.nodes(True):
        node_data = []
        node_data.append(node[0])
        node_data.append(node[1]["node_type"])
        node_info.append(node_data)
    nodes_list = np.array(DG.nodes).tolist()
    text_list = [node if node in cso_cluster else "" for node in nodes_list]
    color = [
        0
        if v == "CSO"
        else 1
        if v == "PumpingStation"
        else 2
        if v == "WaterRecyclingCentre"
        else 3
        for v in list(nx.get_node_attributes(DG, "node_type").values())
    ]

    node_trace = go.Scatter(
        x=node_x,
        y=node_y,
        mode="markers+text",
        hovertext=node_info,
        hoverinfo="text",
        marker=dict(
            color=color,
            colorscale=px.colors.qualitative.D3,
            opacity=node_types,
            size=10,
        ),
        text=text_list,
        textposition="top center",
        textfont_size=15,
    )

    fig = go.Figure(
        data=node_trace,
        layout=go.Layout(
            margin=dict(b=0, l=0, r=0, t=0),
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
            yaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
        ),
    )

    # arrows rather than lines required annotations
    edge_start = []
    edge_end = []
    for edge in DG.edges():
        x0, y0 = DG.nodes[edge[0]]["x"], DG.nodes[edge[0]]["y"]
        x1, y1 = DG.nodes[edge[1]]["x"], DG.nodes[edge[1]]["y"]
        edge_start.append([x0, y0])
        edge_end.append([x1, y1])

    for i in range(len(edge_start)):
        fig.add_annotation(
            ax=edge_start[i][0],  # arrows' tail
            ay=edge_start[i][1],  # arrows' tail
            x=edge_end[i][0],  # arrows' head
            y=edge_end[i][1],  # arrows' head
            xref="x",
            yref="y",
            axref="x",
            ayref="y",
            text="",  # if you want only the arrow
            showarrow=True,
            arrowhead=2,
            arrowsize=2,
            arrowwidth=1,
            arrowcolor="orange",
            opacity=0.7,
        )

    # to set appropriately zoomed in axis on cluster
    cluster_x = []
    cluster_y = []
    for node in cso_cluster:
        cluster_x.append(DG.nodes[node]["x"])
        cluster_y.append(DG.nodes[node]["y"])

    axis_buffer = 250
    fig.update_xaxes(
        range=[min(cluster_x) - axis_buffer, max(cluster_x) + axis_buffer],
        showgrid=False,
    )
    fig.update_yaxes(
        range=[min(cluster_y) - axis_buffer, max(cluster_y) + axis_buffer],
        showgrid=False,
    )

    fig.update_layout(
        height=195,
        width=600,
    )
    return fig