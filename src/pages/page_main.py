import streamlit as st
from st_aggrid import AgGrid, GridOptionsBuilder, GridUpdateMode

from model.features import get_levels, get_sites
from model.plot import (
    draw_correlation_table,
    plot_cso_levels,
    plot_map_with_proposed,
    plot_network,
)
from setup.database import db_con


def run_page_main():
    """Run main page"""
    db = db_con()
    sites = get_sites()[0]
    with st.sidebar.form(key="Form1"):
        time_select = st.selectbox("Select time", ["60m", "15m"])
        connected_select = st.selectbox("Select connectivity", ["All", "Connected"])
        metric_select = st.selectbox("Select metric", ["Pearson", "Spearman"])
        default_cutoff = 0.85 if time_select == "60m" else 0.75
        cutoff_select = st.number_input(
            "Select metric cutoff", min_value=-1.0, max_value=1.0, value=default_cutoff
        )
        graph_select = st.selectbox("Select level view", ["Standard", "Differenced"])

    ############ Correlation dataframe ##################
    # output below from differenced analysis
    df_corr, proposed_sensors = draw_correlation_table(
        time_select, connected_select, metric_select, cutoff_select
    )
    df_corr = df_corr.drop("DTW", axis=1)
    #####################################################

    st.success(
        f"""
    {len(proposed_sensors)} sensors nominated for investigation/removal ({int(100*len(proposed_sensors)/len(sites))}% of total sensors).
    \n `{", ".join(x for x in sorted(proposed_sensors))}`.
    """
    )

    c0_1, c0_2 = st.columns((2, 1))
    with c0_1:
        gb = GridOptionsBuilder.from_dataframe(df_corr)
        gb.configure_selection("single")
        grid_response = AgGrid(
            df_corr,
            gridOptions=gb.build(),
            height=305,
            update_mode=GridUpdateMode.SELECTION_CHANGED,
            fit_columns_on_grid_load=True,
        )

    selected = grid_response["selected_rows"]
    if len(selected) > 0:
        selected = grid_response["selected_rows"]
    else:
        selected = [
            {
                "CSO 1": "16067",
                "CSO 2": "16068",
                "Connect": 1,
                "Dist (km)": 0.22,
                "Pearson": 0.97,
                "Spearman": 0.91,
                "Min Connect": "16068",
            }
        ]

    ################### Clicked data ####################
    selected_1 = selected[0]["CSO 1"]
    selected_2 = selected[0]["CSO 2"]
    cso_select = [selected_1, selected_2]
    df = get_levels(time_select, selected_1, selected_2)
    df_diff = df.diff()
    #####################################################

    #################### Figures ########################
    fig_removal_map = plot_map_with_proposed(
        df_corr, metric_select, cutoff_select, cso_select
    )
    fig_network_0 = plot_network(selected_1)
    fig_network_1 = plot_network(selected_2)
    fig_df = plot_cso_levels(df, cso_select)
    fig_df_diff = plot_cso_levels(df_diff, cso_select)
    #####################################################

    with c0_2:
        st.plotly_chart(fig_removal_map, use_container_width=True)

    st.markdown(
        f"""
    Nodal separation: `{selected[0]["Connect"]}` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
    Distance: `{selected[0]["Dist (km)"]}km` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
    Pearson: `{selected[0]["Pearson"]}` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
    Spearman: `{selected[0]["Spearman"]}` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
    """
    )

    c1_1, c1_2 = st.columns((3, 2))
    with c1_1:
        with st.expander("CSO Levels", expanded=True):
            if len(cso_select) > 0:
                if graph_select == "Standard":
                    st.plotly_chart(fig_df, use_container_width=True)
                elif graph_select == "Differenced":
                    st.plotly_chart(fig_df_diff, use_container_width=True)
    with c1_2:
        st.plotly_chart(fig_network_0, use_container_width=True)
        st.plotly_chart(fig_network_1, use_container_width=True)
