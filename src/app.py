import streamlit as st

from pages.page_main import run_page_main
from setup.layout import setup_page


setup_page("Sensor Location Optimisation")
run_page_main()

with st.sidebar:
    st.header("")
    st.info(
        """
    ### :information_source: Info
    - Statistical techniques used to compare sensor levels.
    - For high correlations, the least connected sensor can be considered for removal.
    """
    )
