sys.path.append('../src/')
import process.clustering_utils as clustering


def format_2series_data_set(df, col_list, scaler = 'mean_var'):
    """Formats dataset and scales to """
    
    node_id = pairings['CSO 1'][i]
    neighbour = pairings['CSO 2'][i]
    
    formatted_dataset = to_time_series_dataset([df[i].values for i in col_list])
    
    X_train = formatted_dataset
    if scaler == 'mean_var':
        print("scale use", scaler)
        X_train = TimeSeriesScalerMeanVariance().fit_transform(X_train)
    else:
        print("no scaler")
    #X_train = TimeSeriesScalerMeanVariance().fit_transform(X_train[:50])
    # Make time series shorter
    X_train = TimeSeriesResampler().fit_transform(X_train)
    return X_train 

dic2 = {}

for i in range(0, len(pairings)):
    node_id = pairings['CSO 1'][i]
    neighbour = pairings['CSO 2'][i]
    # get the timeseries data 
    
    #ts1 = df[node_id]
    #ts2 = df[neighbour]
    
    formatted_dataset = to_time_series_dataset([df[i].values for i in [node_id,  neighbour]])
    
    X_train = formatted_dataset
    X_train = TimeSeriesScalerMeanVariance().fit_transform(X_train)
    
    compare_2 = X_train
    
    comparing = [node_id,  neighbour]
    
    dtw_score = clustering.get_dtw_score(compare_2) 
    
    dic2[node_id + "_" + neighbour] = dtw_score
    
    
    
pairings['map_pair'] = pairings['CSO 1'] + "_" + pairings['CSO 2']
pairings['dtw_score'] = pairings['map_pair'].map(dic2)
pairings.drop(['map_pair'], inplace = True, axis = 1)
pairings 